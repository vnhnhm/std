require 'test_helper'

class BoxesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @box = boxes(:one)
  end

  test "should get index if authorization is present" do
    get boxes_url, headers: { 'Authorization' => 'bla' }
    assert_response :success
    body = JSON.parse(response.body)
    assert_equal 1, body['id']
    assert_equal 'the first one', body['name']
  end

  test "should get index if authorization is not present" do
    get boxes_url, headers: { 'Authorization' => nil }
    assert_response :unauthorized
    body = JSON.parse(response.body)
    assert_equal 'no auth', body['error']
  end

  # test "should get new" do
  #   get new_box_url
  #   assert_response :success
  # end

  # test "should create box" do
  #   assert_difference('Box.count') do
  #     post boxes_url, params: { box: { name: @box.name } }
  #   end

  #   assert_redirected_to box_url(Box.last)
  # end

  # test "should show box" do
  #   get box_url(@box)
  #   assert_response :success
  # end

  # test "should get edit" do
  #   get edit_box_url(@box)
  #   assert_response :success
  # end

  # test "should update box" do
  #   patch box_url(@box), params: { box: { name: @box.name } }
  #   assert_redirected_to box_url(@box)
  # end

  # test "should destroy box" do
  #   assert_difference('Box.count', -1) do
  #     delete box_url(@box)
  #   end

  #   assert_redirected_to boxes_url
  # end
end
